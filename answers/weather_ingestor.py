import sys, os
import pandas
import time
import logging, traceback
from pathlib import Path
from itertools import islice

# Create and configure logger
logger = logging.getLogger()
logging.basicConfig(filename="weather_ingest.log", format='%(filename)s: %(message)s', filemode='w+')
logger.setLevel(logging.DEBUG)

# Configure django environment
cwd = Path(os.getcwd())
djangoDirectory = os.path.join(cwd.parent.absolute(), 'src', 'WeatherYieldAnalyzer')
sys.path.append(djangoDirectory)
os.environ['DJANGO_SETTINGS_MODULE'] = 'WeatherYieldAnalyzer.settings'
import django
django.setup()
from api.models import WeatherStation, ClimateReport

# Using static classes since I see little benefit in
# holding persistent classes in memory to accomplish any of the desired functionality
class ReportsIngestor():
    # Function to ingest single file
    def ingestFile(fileHandle, stationID=None):
        logger.info(f"Ingesting data file: {fileHandle}")

        # If no station ID was given, try to take it from the file path
        if not stationID:
            stationID = os.path.split(fileHandle)[1].split(".")[0]
        
        # If a valid station ID could not be found, log error and quit
        if stationID is None: 
            logger.error(f"A valid station ID could not be determined for file: {fileHandle}")
            return

        # Each line in the file contains 4 records separated by tabs
        # ingest file and scale units to real values 
        fields = ["Date", "MaxTemperature", "MinTemperature", "Precipitation"]
        df = pandas.read_csv(fileHandle, sep='\t',  header=None, names=fields, na_values=[-9999])
        df['MaxTemperature'] = df['MaxTemperature'] / 10
        df['MinTemperature'] = df['MinTemperature'] / 10
        df['Precipitation'] = df['Precipitation'] / 10
        
        # Convert datetime row to datetime objects
        df["Date"] = pandas.to_datetime(df["Date"], format='%Y%m%d')
        
        # If the station ID relates to a new station, one will be made
        # If not, records will just be updated
        station, isNewStations = WeatherStation.objects.update_or_create( stationID=stationID )
        if (isNewStations): logger.info(f"Created station: {stationID}")
        else: logger.info(f"Updating entries for station: {stationID}")

        # Cast rows in dataframe to ClimateReport objects
        weatherReports = [
                ClimateReport(station = station, observationDate = entry[0], maxTemperature = entry[1], minTemperature = entry[2], precipitation = entry[3])
                for entry in df[fields].to_numpy() 
        ]
        
        # Bulk create objects, if there is a conflict, then the entry has already been made.
        batchSize = 800
        objsMade = 0
        while True:
            # Grab the first batch
            batch = weatherReports[0:batchSize]
            # print(batch)
            objsMade += len(ClimateReport.objects.bulk_create(
                batch,
                batchSize,
                ignore_conflicts =True))

            # Toss the first <batchSize> number of elements
            # since they were just added
            weatherReports = weatherReports[batchSize:]

            # If we're done creating item, stop
            if len(weatherReports) == 0: break
        
        # Log the final number of objects made/updated
        logger.info(f"{objsMade} weather report entries added/updated.")


    # Function to ingest entire directory
    def ingestDirectory(directory):
        # Reads directory to get eligible files
        # calls file ingestor for each file
        for file in os.listdir(directory):

            # If the entry is not a file, skip it
            fileHandle = os.path.join(directory, file)
            if not os.path.isfile(fileHandle): continue
            
            # Ingest the file
            try:
                ReportsIngestor.ingestFile(fileHandle)
            except Exception as e:
                logging.error(traceback.format_exc())


if __name__ == '__main__':
    # Start timing operations
    start = time.time()

    # Parse command line args to determine actions
    if len(list(sys.argv)) <= 1:
        logger.error("Data path not specified")
        exit()
    
    # TODO: add help message/cli options
    dataPath = list(sys.argv)[1]

    # If path is a directory, try to ingest all files in directory
    if os.path.isdir(dataPath):  
        logger.info(f"Ingesting data directory: {dataPath}")
        ReportsIngestor.ingestDirectory(dataPath)
    
    # Else, try to ingest a single file
    elif os.path.isfile(dataPath):  
        try:
            ReportsIngestor.ingestFile(dataPath)
        except Exception as e:
            logging.error(traceback.format_exc())

    # If neither worked, log error and terminate
    else:
        logger.error("Invalid or locked path given")

    # Log completion time
    end = time.time()
    logger.info(f"Elapsed run time: {end - start}")
