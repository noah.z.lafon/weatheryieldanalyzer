import sys, os
import pandas
from datetime import datetime
import time
import logging, traceback
from pathlib import Path

# Create and configure logger
logger = logging.getLogger()
logging.basicConfig(filename="yield_ingest.log", format='%(filename)s: %(message)s', filemode='w+')
logger.setLevel(logging.DEBUG)


# Configure django environment
cwd = Path(os.getcwd())
djangoDirectory = os.path.join(cwd.parent.absolute(), 'src', 'WeatherYieldAnalyzer')
sys.path.append(djangoDirectory)
os.environ['DJANGO_SETTINGS_MODULE'] = 'WeatherYieldAnalyzer.settings'
import django
django.setup()
from api.models import CropYield

# Using static classes since I see little benefit in
# holding persistent classes in memory to accomplish any of the desired functionality
class YieldIngestor():

    # Function to ingest single file
    def ingestFile(fileHandle):
        logger.info(f"Ingesting data file: {fileHandle}")

        # Ingest file
        fields = ["Year", "Yield"]
        df = pandas.read_csv(fileHandle, sep='\t',  header=None, names=fields, na_values=[-9999])

        # Convert datetime row to datetime objects and scale yield
        # to reflect actual units
        df["Year"] = pandas.to_datetime(df["Year"], format='%Y')
        df["Yield"] = df["Yield"] * 1000

        # Cast each row in the dataframe to a Django yield model
        # Note: this could be made faster with vectorization if needed.
        yields = [
            CropYield(year = entry[0], yieldWeight = entry[1])
            for entry in df[fields].to_numpy()
        ]

        # Bulk create objects, if there is a conflict, then the model
        # has already been made.
        # If update functionality is needed, we can extend the bulk_create function. 
        objsMade = CropYield.objects.bulk_create(yields,  ignore_conflicts=True)
        logger.info(f"{len(objsMade)} yield entries added to database.")

    # Function to ingest entire directory
    def ingestDirectory(directory):
        # Reads directory to get eligible files
        # calls file ingestor for each file
        for file in os.listdir(directory):

            # If the entry is not a file, skip it
            fileHandle = os.path.join(directory, file)
            if not os.path.isfile(fileHandle): continue
            
            # Ingest the file
            try:
                YieldIngestor.ingestFile(fileHandle)
            except Exception as e:
                logging.error(traceback.format_exc())



if __name__ == '__main__':
    # Start timing operations
    start = time.time()

    # Parse command line args to determine actions
    if len(list(sys.argv)) <= 1:
        logger.error("Data path not specified")
        exit()
    
    # TODO: add help message/cli options
    dataPath = list(sys.argv)[1]

    # If path is a directory, try to ingest all files in directory
    if os.path.isdir(dataPath):  
        logger.info(f"Ingesting data directory: {dataPath}")
        YieldIngestor.ingestDirectory(dataPath)
    
    # Else, try to ingest a single file
    elif os.path.isfile(dataPath):  
        try:
            YieldIngestor.ingestFile(dataPath)
        except Exception as e:
            logging.error(traceback.format_exc())

    # If neither worked, log error and terminate
    else:
        logger.error("Invalid or locked path given")

    # Log completion time
    end = time.time()
    logger.info(f"Elapsed run time: {end - start}")
