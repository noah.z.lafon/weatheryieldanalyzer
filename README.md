# Setup
This project was built on django 4.1.3 in python3.8+ using a sqlite3 database.

After those dependencies are installed, install the following with the python pip package manager:
```
pip install djangorestframework
pip install markdown
pip install django-filter
pip install pandas
```

# Ingesting files:
Each ingestor can be passed either a file path or file directory. Providing a directory will result in the ingestor attempting to ingest all files in that directory.
```
$ python yield_ingestor.py ../yld_data/US_corn_grain_yield.txt 
$ python weather_ingestor.py ../wx_data/
```
Each ingestor generates an ingestion log in their working directory with the total ingestion time. An example can be found at: ```/answers/weather_ingest.log```.

# Viewing reports
- Yield data can be viewed at ```/api/yield```
- Reports data can be viewed at ```/api/weather```
- Statistical data can be viewed at ```/api/weather/stats```

## Filtering
When applicable, the API data can be filtered or navigated by use of URL path variables.
For example, to filter the weather reports by a given station, use __?station__ like so ```/api/weather?station=USC00116579```.

To filter by a comparison, state the field desired to filter against followed by double underscores with the desired comparison constraint. Example: ```api/weather?observationDate__lte=1985-01-05```

Example Filter modifiers:
| Symbol Constraint | Operation |
|--------|------------|
| __lt   | <          |
| __lte  | <=         |
| __gt   | >          |
| __gte  | >=         |




## Navigation
By default, all queries will be constrained to a 100 model limit. If all the desired data can't be viewed in a single query, appending an __offset__ will show the corresponding page. For example, to get the second page of weather reports that are less than or equal to a given date, form the following:
```api/weather/?limit=100&observationDate__lte=1985-01-05&offset=100```.

# Example Results
### Requesting 5 entries less than or equal to a given date:
- ```GET http://127.0.0.1:8000/api/weather?observationDate__lte=1985-01-05&limit=5```
- ![5_entries_lte_date_ex](/answers/get_5_lte_date_result.PNG)

### Requesting Statistics for a specific weather station (image truncated):
- ```GET http://127.0.0.1:8000/api/weather?observationDate__lte=1985-01-05&limit=5```
- ![5_entries_lte_date_ex](/answers/get_station_stats.PNG)


# Models
There are 3 data models. WeatherStation, ClimateReport, and CropYield.
Each ClimateReport has some parent WeatherStation along with its report stats.
The CropYields each have a year and a yield value.
The WeatherStations each have implicit data about their reports over time.

If performance were to become an issue when querying statistical data, caching techniques could easily be implemented. Additionally, the Django migrations classes could be extended to trigger updates to a sum and count field (like total max temperature and number of valid entries) for each statistic allowing them to be converted to their aggregate values in constant time.

# Database choice
Since I had very limited personal time I could commit to this coding challenge, I went with what would get the best deliverable for the least time. As a result, I implemented the data models in sqlite3.
However, the data modeled is time series with implicit geospatial information. If the geospatial data ever needed to be realized, I would rather utilize PostgreSQL. There are many options/tools built around supporting the functionality of time series and geospatial information for PostgreSQL.

# Project Structure
- /answers directory contains the ingestor code.
- /src contains the Django project
  - Data models can be found in /src/WeatherYieldAnalyzer/api/models.py
  - Data models serializers can be found in /src/WeatherYieldAnalyzer/api/serializers.py
  - API views can be found in /src/WeatherYieldAnalyzer/api/views.py

# TODO/Improvements
Below is a list of ways this project could be improved.
I’d be happy to talk through the technologies/techniques I'd utilize to accomplish these as well as the reasons and motivations for the improvements.
- Extend the ingestor to inherit common functionality from a parent ingestor class.
- Vectorize ingestor list comprehension for increased performance.
- Add more robust error handling to the API endpoints.
- Implement caching techniques to increase API response times and scalability.
- Create thorough API documentation.
- Write API endpoint and filtering tests.
- Refine the objects.bulk_create methods to allow for specific update functionality.
