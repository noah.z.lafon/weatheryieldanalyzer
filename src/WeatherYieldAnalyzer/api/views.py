from rest_framework.pagination import LimitOffsetPagination
from rest_framework.generics import ListAPIView, ListCreateAPIView
from django_filters.rest_framework import DjangoFilterBackend, FilterSet

# Models and serializers
from api.models import WeatherStation, ClimateReport, CropYield
from .serializers import StationSerializer, ReportSerializer, YieldSerializer


class Reports(ListCreateAPIView):
    """ View to allow for filtering and viewing weather reports"""
    queryset = ClimateReport.objects.all()
    serializer_class = ReportSerializer
    pagination_class = LimitOffsetPagination

    # Filter reports by date or station ID
    filterset_fields = {
     'observationDate':['gte', 'lte', 'exact', 'gt', 'lt'],
     'station':['exact'],
    }


class StationStats(ListCreateAPIView):
    """ View to allow for filtering and viewing weather station stats"""
    queryset = WeatherStation.objects.all()
    serializer_class = StationSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['stationID']


class YieldView(ListCreateAPIView):
    """ View to allow for filtering and viewing yield information"""
    queryset = CropYield.objects.all() 
    serializer_class = YieldSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]

    # Allows for filtering by year and yield weight
    filterset_fields = {
     'year':['gte', 'lte', 'exact', 'gt', 'lt'],
     'yieldWeight':['gte', 'lte', 'exact', 'gt', 'lt'],
    }