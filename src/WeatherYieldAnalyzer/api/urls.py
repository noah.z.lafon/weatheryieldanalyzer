from django.urls import path
from . import views

urlpatterns = [
    path('weather/', views.Reports.as_view(), name='reports'),
    path('yield/', views.YieldView.as_view(), name='yield'),
    path('weather/stats/', views.StationStats.as_view(), name='yield'),    
]
