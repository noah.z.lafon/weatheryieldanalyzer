from rest_framework import serializers
from api.models import WeatherStation, ClimateReport, CropYield

# These classes outline how to serialize each model

class StationSerializer(serializers.ModelSerializer):
    # Meta fields should be read only
    getStatsByYear = serializers.ReadOnlyField()
    # getStationStats = serializers.ReadOnlyField()

    class Meta:
        model = WeatherStation
        fields = ('stationID', 'getStatsByYear')#, 'getStationStats')
    

class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClimateReport
        fields = ('maxTemperature', 'id', 'minTemperature', 'precipitation', 'station', 'observationDate')

class YieldSerializer(serializers.ModelSerializer):
    class Meta:
        model = CropYield
        fields = ('year', 'yieldWeight')