from django.db import models
from django.db.models import Avg, Sum

# Weather station model
class WeatherStation(models.Model):
    stationID = models.CharField(max_length=30, primary_key=True)

    @property
    def getStationStats(self):
        # Returns aggregate statistics for this station
        stationReports = ClimateReport.objects.filter(station=self.stationID)
        stats = stationReports.aggregate(Avg('maxTemperature'), Avg('minTemperature'), Sum('precipitation'))
        return stats
    
    @property
    def getStatsByYear(self):
        # Group reports by year then compute aggregate statistics
        return ClimateReport.objects.values('observationDate__year').annotate(avgMax=Avg('maxTemperature')).annotate(avgMin=Avg('minTemperature')).annotate(precipSum=Sum('precipitation'))

# Observations
class ClimateReport(models.Model):
    # The station this observation was made at
    station = models.ForeignKey(WeatherStation, on_delete=models.CASCADE)

    # The day the observation was made
    observationDate = models.DateField()

    # Measurement taken
    maxTemperature = models.FloatField()
    minTemperature = models.FloatField()
    precipitation = models.FloatField()

    def __str__(self):
        return f"{self.observationDate}_{self.station.stationID}_{self.maxTemperature}"

    # Adds constrain to protect against duplicate data corrupting statistics
    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['station', 'observationDate'], name='unique_station_observationDate'
            )
        ]

# Crop yield data, each year should be unique so use that as primary key
class CropYield(models.Model):
    year = models.DateField(primary_key=True)
    yieldWeight = models.IntegerField()

